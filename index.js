const express = require('express')
const app = express()
const mongoose = require("mongoose")
const bodyParser = require('body-parser')
const logger = require("morgan")

app.use(logger("dev"))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  autoReconnect: true,
  replicaSet: 'rs0',
  connectWithNoPrimary: true,
};

var userSchema = new mongoose.Schema({
  firstName: String,
  lastName: String
});

var User = mongoose.model("User", userSchema);


mongoose.connect("mongodb://mongo1:27017,mongo2:27017,mongo3:27017/my_database?replicaSet=rs0", options)
  .then(() => {
    console.log("connected to mongo")
  });

app.set("view engine", "ejs")

app.get("/", (req, res, next) => {
  res.status(200).render('index')
})

app.post("/addname", (req, res) => {
  var myData = new User(req.body);
  myData.save()
    .then(item => {
      res.status(200).send("user saved to database");
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });
});

app.get("/users", (req, res, next) => {
  User.find({}, function (err, users) {
    res.status(200).json({ users: users });
  });
})

app.listen(5000, () => console.log("Server is running at port 5000"))
